// El modulo importado en la suguiente línea es el que debes crear.
const createServer = require("./create_server.js");

function getHtml(num) {
    return `<!DOCTYPE html>
    <html lang="en">
    
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>Tic Tac Toe</title>
    </head>
    
    <body>
        <center>
            <div id="root">
                <h1><span fw-content="header"></span></h1>
    
                <div id="fila1">
                    <button id="boton0" class="boton" onclick="functionBoton(0)" type="button">.</button>
                    <button id="boton1" class="boton" onclick="functionBoton(1)" type="button">.</button>
                    <button id="boton2" class="boton" onclick="functionBoton(2)" type="button">.</button>
                </div>
                <div id="fila2">
                    <button id="boton3" class="boton" onclick="functionBoton(3)" type="button">.</button>
                    <button id="boton4" class="boton" onclick="functionBoton(4)" type="button">.</button>
                    <button id="boton5" class="boton" onclick="functionBoton(5)" type="button">.</button>
    
                </div>
                <div id="fila3">
                    <button id="boton6" class="boton" onclick="functionBoton(6)" type="button">.</button>
                    <button id="boton7" class="boton" onclick="functionBoton(7)" type="button">.</button>
                    <button id="boton8" class="boton" onclick="functionBoton(8)" type="button">.</button>
    
                </div>
            </div>
            <span id="numPartidas"></span>
            <div id="cajaReinicio">
                <button id="botonReinicio" class="boton" onclick="functionBotonReinicio()" type="button"><b>Volver a
                        jugar</b></button>
                <button id="botonSonido" onclick="functionBotonSonido()" type="button">NianCat<b></b></button>
            </div>
            <div>
                <span id="labelVictoria"></span>
            </div>
        </center>
    
        <style>
            .boton {
                width: 50px;
                height: 50px;
            }
    
            #botonReinicio {
                width: 80px;
                height: 40px;
            }
        </style>
    
        <script>
            function ponerNumPartidas() {
                var num = ` + num + `;
                document.getElementById("numPartidas").innerHTML = "Nº Partidas:" + num;
            }
        </script>
        <script>
        var audio = new Audio('D:/Practica 4 Programacion web - Sergio Dominguez/Beauty Of Annihilation.mp3');
            var tabla = [[0, 0, 0], [0, 0, 0], [0, 0, 0]];
            var jugando = true;
            ponerNumPartidas();
        </script>
        <script>
            var sonar = true;
            function functionBotonSonido() {
                if (sonar) {
                    audio.play();
                    sonar = false;
                } else {
                    audio.pause();
                    sonar = true;
                }
            }
        </script>
        <script>
            function functionBotonReinicio() {
                location.reload();
            }
        </script>
        <script>
            function comprobarVictoria(t) {
                if (tabla[0][0] === tabla[1][0] && tabla[1][0] === tabla[2][0]) {
                    if (tabla[0][0] != 0) {
                        return "v0";
                    }
                }
                if (tabla[0][1] === tabla[1][1] && tabla[1][1] === tabla[2][1]) {
                    if (tabla[0][1] != 0) {
                        return "v1";
                    }
                }
                if (tabla[0][2] === tabla[1][2] && tabla[1][2] === tabla[2][2]) {
                    if (tabla[0][2] != 0) {
                        return "v2";
                    }
                }
                if (tabla[0][0] === tabla[0][1] && tabla[0][1] === tabla[0][2]) {
                    if (tabla[0][0] != 0) {
                        return "h0";
                    }
                }
                if (tabla[1][0] === tabla[1][1] && tabla[1][1] === tabla[1][2]) {
                    if (tabla[1][0] != 0) {
                        return "h1";
                    }
                }
                if (tabla[2][0] === tabla[2][1] && tabla[2][1] === tabla[2][2]) {
                    if (tabla[2][0] != 0) {
                        return "h2";
                    }
                }
                if (tabla[0][0] === tabla[1][1] && tabla[1][1] === tabla[2][2]) {
                    if (tabla[0][0] != 0) {
                        return "ii";
                    }
                }
                if (tabla[0][2] === tabla[1][1] && tabla[1][1] === tabla[2][0]) {
                    if (tabla[0][2] != 0) {
                        return "/";
                    }
                }
                return false;
            }
        </script>
        <script>
            function elegirCasilla(t) {
                var posiblesCasillas = [0, 1, 2, 3, 4, 5, 6, 7, 8]; //0 -> nada, 1 -> usuario, 2 -> 'IA'
    
                var i = 0;
                for (fila of tabla) {
                    for (elemento of fila) {
                        if (elemento != 0) {
                            posiblesCasillas.splice(i, 1);
                            i--;
                        }
                        i++;
                    }
                }
                return posiblesCasillas[Math.floor(Math.random() * posiblesCasillas.length)];
            }
        </script>
        <script>
            function functionBoton(n) {
                if (!jugando) {
                    return;
                }
                var fila = 0;
                var columna = 0;
                switch (n) {
                    case (0):
                        break;
                    case (1):
                        fila = 0;
                        columna = 1;
                        break;
                    case (2):
                        fila = 0;
                        columna = 2;
                        break;
                    case (3):
                        fila = 1;
                        columna = 0;
                        break;
                    case (4):
                        fila = 1;
                        columna = 1;
                        break;
                    case (5):
                        fila = 1;
                        columna = 2;
                        break;
                    case (6):
                        fila = 2;
                        columna = 0;
                        break;
                    case (7):
                        fila = 2;
                        columna = 1;
                        break;
                    case (8):
                        fila = 2;
                        columna = 2;
                        break;
                }
                if (tabla[fila][columna] != 0) {
                    return;
                }
                document.getElementById("boton" + n).innerHTML = "X";
                document.getElementById("boton" + n).style = "background-color: green";
    
                tabla[fila][columna] = 1;
                switch (comprobarVictoria(tabla)) {
                    case ("h0"):
                        document.getElementById("boton0").style = "background-color: MediumSeaGreen";
                        document.getElementById("boton1").style = "background-color: MediumSeaGreen";
                        document.getElementById("boton2").style = "background-color: MediumSeaGreen";
                        jugando = false;
                        victoria(true);
                        break;
                    case ("h1"):
                        document.getElementById("boton3").style = "background-color: MediumSeaGreen";
                        document.getElementById("boton4").style = "background-color: MediumSeaGreen";
                        document.getElementById("boton5").style = "background-color: MediumSeaGreen";
                        jugando = false;
                        victoria(true);
                        break;
                    case ("h2"):
                        document.getElementById("boton6").style = "background-color: MediumSeaGreen";
                        document.getElementById("boton7").style = "background-color: MediumSeaGreen";
                        document.getElementById("boton8").style = "background-color: MediumSeaGreen";
                        jugando = false;
                        victoria(true);
                        break;
                    case ("v0"):
                        document.getElementById("boton0").style = "background-color: MediumSeaGreen";
                        document.getElementById("boton3").style = "background-color: MediumSeaGreen";
                        document.getElementById("boton6").style = "background-color: MediumSeaGreen";
                        jugando = false;
                        victoria(true);
                        break;
                    case ("v1"):
                        document.getElementById("boton1").style = "background-color: MediumSeaGreen";
                        document.getElementById("boton4").style = "background-color: MediumSeaGreen";
                        document.getElementById("boton7").style = "background-color: MediumSeaGreen";
                        jugando = false;
                        victoria(true);
                        break;
                    case ("v2"):
                        document.getElementById("boton2").style = "background-color: MediumSeaGreen";
                        document.getElementById("boton5").style = "background-color: MediumSeaGreen";
                        document.getElementById("boton8").style = "background-color: MediumSeaGreen";
                        jugando = false;
                        victoria(true);
                        break;
                    case ("ii"):
                        document.getElementById("boton0").style = "background-color: MediumSeaGreen";
                        document.getElementById("boton4").style = "background-color: MediumSeaGreen";
                        document.getElementById("boton8").style = "background-color: MediumSeaGreen";
                        jugando = false;
                        victoria(true);
                        break;
                    case ("/"):
                        document.getElementById("boton2").style = "background-color: MediumSeaGreen";
                        document.getElementById("boton4").style = "background-color: MediumSeaGreen";
                        document.getElementById("boton6").style = "background-color: MediumSeaGreen";
                        jugando = false;
                        victoria(true);
                        break;
                }
                if (!jugando) {
                    return;
                }
                //se ha hecho clic pero aun no se ha ganado:      
                var casilla = elegirCasilla(tabla);
                switch (casilla) {
                    case (0):
                        tabla[0][0] = 2;
                        break;
                    case (1):
                        tabla[0][1] = 2;
                        break;
                    case (2):
                        tabla[0][2] = 2;
                        break;
                    case (3):
                        tabla[1][0] = 2;
                        break;
                    case (4):
                        tabla[1][1] = 2;
                        break;
                    case (5):
                        tabla[1][2] = 2;
                        break;
                    case (6):
                        tabla[2][0] = 2;
                        break;
                    case (7):
                        tabla[2][1] = 2;
                        break;
                    case (8):
                        tabla[2][2] = 2;
                        break;
                }
                document.getElementById("boton" + casilla).innerHTML = "O";
                document.getElementById("boton" + casilla).style = "background-color: red";
                // se mira si se ha perdido
                switch (comprobarVictoria(tabla)) {
                    case ("h0"):
                        document.getElementById("boton0").style = "background-color: Tomato";
                        document.getElementById("boton1").style = "background-color: Tomato";
                        document.getElementById("boton2").style = "background-color: Tomato";
                        jugando = false;
                        victoria(false);
                        break;
                    case ("h1"):
                        document.getElementById("boton3").style = "background-color: Tomato";
                        document.getElementById("boton4").style = "background-color: Tomato";
                        document.getElementById("boton5").style = "background-color: Tomato";
                        jugando = false;
                        victoria(false);
                        break;
                    case ("h2"):
                        document.getElementById("boton6").style = "background-color: Tomato";
                        document.getElementById("boton7").style = "background-color: Tomato";
                        document.getElementById("boton8").style = "background-color: Tomato";
                        jugando = false;
                        victoria(false);
                        break;
                    case ("v0"):
                        document.getElementById("boton0").style = "background-color: Tomato";
                        document.getElementById("boton3").style = "background-color: Tomato";
                        document.getElementById("boton6").style = "background-color: Tomato";
                        jugando = false;
                        victoria(false);
                        break;
                    case ("v1"):
                        document.getElementById("boton1").style = "background-color: Tomato";
                        document.getElementById("boton4").style = "background-color: Tomato";
                        document.getElementById("boton7").style = "background-color: Tomato";
                        jugando = false;
                        victoria(false);
                        break;
                    case ("v2"):
                        document.getElementById("boton2").style = "background-color: Tomato";
                        document.getElementById("boton5").style = "background-color: Tomato";
                        document.getElementById("boton8").style = "background-color: Tomato";
                        jugando = false;
                        victoria(false);
                        break;
                    case ("ii"):
                        document.getElementById("boton0").style = "background-color: Tomato";
                        document.getElementById("boton4").style = "background-color: Tomato";
                        document.getElementById("boton8").style = "background-color: Tomato";
                        jugando = false;
                        victoria(false);
                        break;
                    case ("/"):
                        document.getElementById("boton2").style = "background-color: Tomato";
                        document.getElementById("boton4").style = "background-color: Tomato";
                        document.getElementById("boton6").style = "background-color: Tomato";
                        jugando = false;
                        victoria(false);
                        break;
                }
            }
        </script>
        <script>
            function sleep(ms) {
                return new Promise(resolve => setTimeout(resolve, ms));
            }
            async function mover(e) {
                var posicion = 30;
                var direccion = true; // true->derecha, false->izq
                while (true) {
                    if (direccion) {
                        // hacia la derecha
                        posicion += 0.5;
                    } else {
                        // hacia la izq
                        posicion += -0.5;
                    }
                    if (posicion >= 50) {
                        direccion = false;
                    }
                    if (posicion <= 20) {
                        direccion = true;
                    }
                    e.style = "font-size: 100px; position: absolute; left: " + posicion + "%";
                    await sleep(30);
                }
            }
            function victoria(si) {
                var span = document.getElementById("labelVictoria");
                if (si) {
                    span.innerHTML = "¡Ganaste!";
                } else {
                    span.innerHTML = "Derrota ;(";
                }
                mover(span);
            }
        </script>
        <script>
            window.createApp = ({ rootElementId, initialState, handlers }) => {
                const iterateChildrens = (childrens, handlers, initialState, parent) => {
                    childrens.map(child => {
                        if (child.attributes) applyAttributes(Array.from(child.attributes), child, handlers, initialState, parent);
                        if (child.children) iterateChildrens(Array.from(child.children), handlers, initialState, parent);
                    });
                }
                const applyAttributes = (attributes, child, handlers, state, parent) => {
                    (attributes).map(attr => {
                        var fields = attr.name.split(':');
    
                        //Returns Returns the name of the attribute. Ex: "fw-on"/"fw-if"/"fw-attr"/....
                        var attributeName = fields[0];
    
                        //Returns the type of the attribute. Ex: "change", "value", ....
                        var type = fields[1];
    
                        //Returns the value of the atribute. Ex: "password", "changePassword", "isPageLogin", ....
                        const value = attr.value;
    
                        switch (attributeName) {
                            case ("fw-content"): {
                                child.innerHTML = state[value]
                                break;
                            }
    
                            case ("fw-if"): {
    
                                if (state[value] === false) child.hidden = true
                                else if (state[value] === true) child.hidden = false
                                break;
                            }
                            case ("fw-attr"): {
                                child.setAttribute(type, state[value])
                                break;
                            }
                            case ("fw-on"): {
    
    
                                //evento pasandole el nombre del evento
                                child.addEventListener(type, (event) => {
                                    //llamar al handler y actualizar estado
                                    state = handlers[value](state, event)
                                    //llamar a la funcion regresiva
                                    iterateChildrens(Array.from(parent.children), handlers, state, parent)
                                });
                                break;
                            }
                            case ("fw-style"): {
                                const attStyle = child.getAttribute("fw-style")
                                switch (type) {
                                    case ("color"):
                                        child.style.color = initialState[attStyle]
                                        break;
                                    case ("type"):
                                        child.style.type = initialState[attStyle]
                                        break;
                                }
                            }
                        }
                    });
                }
                const root = document.getElementById(rootElementId);
                iterateChildrens(Array.from(root.children), handlers, initialState, root);
            }
    
        </script>
        <script>
            window.createApp({
                rootElementId: "root",
                initialState: {
                    header: "Tic Tac Toe",
    
                },
                handlers: {
                },
            });
        </script>
    </body>
    
    </html>`;
}

var usuarios = new Map();

const get = async (request, response) => {
    var usuario = request.headers.Host;
    if (usuarios.get(usuario) === undefined) {
        usuarios.set(usuario, 0);
    }
    var numPartidas = usuarios.get(usuario);
    usuarios.set(usuario, usuarios.get(usuario) + 1);
    console.log("(" + usuario + ") - " + usuarios.get(usuario));
    response.send(
        "200",
        {   // headers:
            "content-Type": "text/html",
            "Content-Security-Policy": "default-src https: 'unsafe-eval' 'unsafe-inline'; object-src 'none'"
        },
        getHtml(numPartidas)
    );
};

const requestListener = (request, response) => {
    switch (request.method) {
        case "GET": {
            return get(request, response);
        }
        default: {
            return response.send(
                404,
                { "Content-Type": "text/plain" },
                "The server only supports HTTP method GET"
            );
        }
    }
};

const server = createServer((request, response) => {
    try {
        return requestListener(request, response);
    } catch (error) {
        console.error(error);
        response.send(500, { "Content-Type": "text/plain" }, "Uncaught error");
    }
});
server.listen(8080);
